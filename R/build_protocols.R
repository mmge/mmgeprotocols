build_protocols <- function(overwrite = FALSE) {

  if(!dir.exists(protocol_path())) {
    dir.create(protocol_path())
  }

  specimen_types <- oncore2::oncore_connect(verbose = TRUE, distinct = TRUE, query_name = "protocols") %>%
    oncore2::select_fields(PROTOCOL_NO, COLLECTION_GROUP) %>%
    oncore2::execute_query() %>%
    dplyr::filter(grepl("^MMGE", PROTOCOL_NO)) %>%
    dplyr::filter(!is.na(COLLECTION_GROUP)) %>%
    dplyr::mutate(COLLECTION_GROUP = ifelse(COLLECTION_GROUP == "BUFFY COAT", "DNA", COLLECTION_GROUP)) %>%
    dplyr::distinct()

  protocols <- unique(specimen_types$PROTOCOL_NO)

  for(protocol in protocols) {

    if(protocol_exists(protocol) && overwrite == FALSE) {
      cat_line(crayon::red(clisymbols::symbol$cross, "Skipping", protocol, "..."))
    } else {
      cat_line(crayon::green(clisymbols::symbol$star, "Adding", protocol, "..."))
      add_protocol(protocol, collection_groups = unique(specimen_types$COLLECTION_GROUP[specimen_types$PROTOCOL_NO == protocol]))
    }

  }

}