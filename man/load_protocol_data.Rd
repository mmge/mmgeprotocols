% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/protocol_data.R
\name{load_protocol_data}
\alias{load_protocol_data}
\title{Automatically read in xlsx and csv files}
\usage{
load_protocol_data(protocol, ..., pattern)
}
\arguments{
\item{protocol}{the name of the protocol}

\item{\dots}{Specific files to load from the data directory}

\item{pattern}{A regular expression to match file names to}
}
\description{
Primarily meant to streamline pullsheet projects by automating the inclusion
of support data into the global environment
}
